#ifndef _PRODUCT
#define _PRODUCT

#include <string>

class Product {
private:
	std::string m_name;
	int m_quantityInStock;
	float m_price;

public:
	Product();
	Product(std::string newName, int newQuantity, int newPrice);
	Product(const Product& other);
	void Setup(std::string newName, int newQuantity, int newPrice);
	void Setup(std::string newName);
	std::string GetName() const;
	int GetQuantityInStock() const;
	float GetPrice() const;
};




#endif
