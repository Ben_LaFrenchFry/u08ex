#include "Product.h"


void Product::Setup(std::string newName, int newQuantity, int newPrice) {
	m_name = newName;
	m_quantityInStock = newQuantity;
	m_price = newPrice;
}

void Product::Setup(std::string newName){
	m_name = newName;
}

Product::Product() {
	std::string name = "UNSET";
	int quantity = 0;
	int price = 0;
	Setup(name, quantity, price);
}

Product::Product(std::string newName, int newQuantity, int newPrice) {
	Setup(newName, newQuantity, newPrice);
}

Product::Product(const Product& copyMe) {
	m_name = copyMe.m_name;
	m_quantityInStock = copyMe.m_quantityInStock;
	m_price = copyMe.m_price;
}

std::string Product::GetName() const {
	return m_name;
}

int Product::GetQuantityInStock() const {
	return m_quantityInStock;
}

float Product::GetPrice() const {
	return m_price;
}
